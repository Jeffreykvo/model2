<?php

namespace Model_setup\Model;

use Illuminate\Database\Eloquent\Model;

class YourModel
{
    //test10
    protected $table = 'YourModel';

    protected $fillable = ['name'];

    public $timestamps = false;
}
