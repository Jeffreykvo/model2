<?php

namespace Model_Setup\Model;

use Illuminate\Support\ServiceProvider;

class ModelServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->publishes([
            __DIR__.'Models/YourModel2.php' => app_path('Models/YourModel2.php'),
        ], 'models');
    }

    public function register()
    {
        //
    }
}
